﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Categories.aspx.cs" Inherits="Categories" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 15 : Northwind</title>
    <link href="Styles/Main.css" rel="stylesheet" />
</head>
<body>
    <header>Manage Your Business The Right Way</header>
    <section>
        <h2>Category Maintainence</h2>

         <form id="form1" runat="server">
             <asp:GridView ID="gvCategories" runat="server" AutoGenerateColumns="False" DataKeyNames="CategoryID" DataSourceID="sdsCategories" OnRowUpdated="gvCategories_RowUpdated" OnRowCancelingEdit="gvCategories_RowCancelingEdit">
                 <Columns>
                     <asp:BoundField DataField="CategoryID" HeaderText="ID" ReadOnly="True" SortExpression="CategoryID" />
                     <asp:TemplateField HeaderText="Name" SortExpression="CategoryName">
                         <EditItemTemplate>
                             <asp:TextBox ID="txtCategoryName" runat="server" Text='<%# Bind("CategoryName") %>' ValidationGroup="vgCategories"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName" CssClass="error" Display="Dynamic" ErrorMessage="This field is required.">*</asp:RequiredFieldValidator>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblCategoryName" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Description" SortExpression="Description">
                         <EditItemTemplate>
                             <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                             <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" CssClass="error" Display="Dynamic" ErrorMessage="This field is required.">*</asp:RequiredFieldValidator>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:CommandField ShowEditButton="True" />
                 </Columns>
             </asp:GridView>

             <asp:SqlDataSource ID="sdsCategories" runat="server" 
                 ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" 
                 DeleteCommand="DELETE FROM [tblCategories]
                    WHERE [CategoryID] = ?" 
                 InsertCommand="INSERT INTO [tblCategories] 
                    ([CategoryID], [CategoryName], [Description]) 
                    VALUES (?, ?, ?)" 
                 ProviderName="<%$ ConnectionStrings:NorthwindConnectionString.ProviderName %>" 
                 SelectCommand="SELECT [CategoryID], [CategoryName], [Description] 
                    FROM [tblCategories]" 
                 UpdateCommand="UPDATE [tblCategories] 
                    SET [CategoryName] = ?, [Description] = ? 
                    WHERE [CategoryID] = ?">
                 <DeleteParameters>
                     <asp:Parameter Name="CategoryID" Type="Int32" />
                 </DeleteParameters>
                 <InsertParameters>
                     <asp:Parameter Name="CategoryID" Type="Int32" />
                     <asp:Parameter Name="CategoryName" Type="String" />
                     <asp:Parameter Name="Description" Type="String" />
                 </InsertParameters>
                 <UpdateParameters>
                     <asp:Parameter Name="CategoryName" Type="String" />
                     <asp:Parameter Name="Description" Type="String" />
                     <asp:Parameter Name="CategoryID" Type="Int32" />
                 </UpdateParameters>
             </asp:SqlDataSource>

             <asp:ValidationSummary ID="vsCategories" CssClass="error" runat="server" HeaderText="Please correct the follwing errors." />

             <p>To create a new category, enter the category information and click "Add New Category"</p>

             <asp:Label ID="lblError" CssClass="error" runat="server"></asp:Label>
        </form>

    </section>
</body>
</html>
