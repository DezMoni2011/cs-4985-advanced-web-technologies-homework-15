﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// The code behind file that handles most events for the Categories page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 19, 2015 | Spring
/// </version>
public partial class Categories : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Handles the RowUpdated event of the gvCategories control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvCategories_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured.<br /><br />"
                                 + "Message : " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        } else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category." +
                                 "<br />Please try again.";
        }
    }
    /// <summary>
    /// Handles the RowCancelingEdit event of the gvCategories control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewCancelEditEventArgs"/> instance containing the event data.</param>
    protected void gvCategories_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.lblError.Text = "";
    }
}